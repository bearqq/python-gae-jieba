#!/usr/bin/env python
#
import os
import webapp2
import logging

#ALLOWED_APP_IDS = ('24ae7c', '2b359e')
from google.appengine.api import memcache
import hashlib
import json

MEMCACHE_TIMEOUT = 60 * 60 * 24


class MainHandler(webapp2.RequestHandler):
	def get(self):
		self.response.write('Hello world!')


class Analyse(webapp2.RequestHandler):
	def handle_requests(self):

		# app_id = self.request.headers.get('X-Appengine-Inbound-Appid', None)
		# logging.info("APPID:%s" % app_id)
		# if app_id in ALLOWED_APP_IDS:
		#     pass
		# else:
		#     self.abort(403)


		text = self.request.get("text", default_value="")
		withWeight = int(self.request.get("withWeight", default_value="1"))
		mode = self.request.get("mode", default_value="TF-IDF")
		topK = int(self.request.get("topK", default_value="20"))
		allowPOS = tuple(self.request.get_all("allowPOS"))
		md5sum = hashlib.md5((''.join(allowPOS) + text + mode + str(withWeight) + str(topK)).encode("utf-8")).hexdigest()
		mark = self.request.get("mark", default_value="")
		rttext = self.request.get("rttext", default_value="")

		json_data = memcache.get('{}:Analyse'.format(md5sum))
		
		data = {'mode': mode, 'topK': topK, 'allowPOS': allowPOS, 'md5sum': md5sum}
		
		if json_data:
			data["result"] = json_data
		else:
			# import jieba.analyse

			# jieba.analyse.set_stop_words(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', 'stop_words.txt'))

			# jieba.dt.tmp_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'tmp')

			if mode == "TF-IDF":
				import jieba.analyse.tfidf
				jieba.dt.tmp_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'tmp')
				default_tfidf = jieba.analyse.tfidf.TFIDF()
				extract_tags = default_tfidf.extract_tags
				default_tfidf.set_stop_words(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', 'stop_words.txt'))
				set_idf_path = default_tfidf.set_idf_path
				data["result"] = extract_tags(text, topK=topK, withWeight=bool(withWeight), allowPOS=allowPOS)
			else:
				import jieba.analyse.textrank
				jieba.dt.tmp_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'tmp')
				default_textrank = jieba.analyse.textrank.TextRank()
				textrank = default_textrank.extract_tags
				default_textrank.set_stop_words(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', 'stop_words.txt'))
				data["result"] = textrank(text, withWeight=bool(withWeight), topK=topK)

			memcache.add('{}:Analyse'.format(md5sum), data["result"], MEMCACHE_TIMEOUT)
		
		if rttext:
			data['text']=text
		if mark:
			data['mark']=mark
		json_data = json.dumps(data)
		
		self.response.headers['Content-Type'] = 'application/json'
		self.response.out.write(json_data)


	def get(self):
		self.handle_requests()

	def post(self):
		self.handle_requests()


class Cut(webapp2.RequestHandler):
	def handle_requests(self):

		# app_id = self.request.headers.get('X-Appengine-Inbound-Appid', None)
		# logging.info("APPID:%s" % app_id)
		# if app_id in ALLOWED_APP_IDS:
		#     pass
		# else:
		#     self.abort(403)

		text = self.request.get("text", default_value="")
		mode = int(self.request.get("cut_all", default_value="0"))
		md5sum = hashlib.md5((text + str(mode)).encode("utf-8")).hexdigest()
		mark = self.request.get("mark", default_value="")
		rttext = self.request.get("rttext", default_value="")

		json_data = memcache.get('{}:Cut'.format(md5sum))
		
		data = {'mode': mode, 'md5sum': md5sum}
		
		if json_data:
			data["result"] = json_data
		else:
			import jieba

			jieba.dt.tmp_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'tmp')

			data["result"] = []

			for x in jieba.cut(text, cut_all=bool(mode)):
				data["result"].append(x)
			
			memcache.add('{}:Cut'.format(md5sum), data["result"], MEMCACHE_TIMEOUT)
		
		if rttext:
			data['text']=text
		if mark:
			data['mark']=mark
			
		json_data = json.dumps(data)
		self.response.headers['Content-Type'] = 'application/json'
		self.response.out.write(json_data)

	def get(self):
		self.handle_requests()

	def post(self):
		self.handle_requests()

class Index(webapp2.RequestHandler):
	def get(self):
		self.response.headers['Content-Type'] = 'text/html'
		self.response.write(u"""\
<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>JB</title>
</head>
<body>
<div>
<h1>cut</h1>
<form action="/cut" enctype="multipart/form-data" method="post">
	<div><label>text</label><textarea name='text'></textarea></div>
	<div><label>cut_all</label><input type="text" name="cut_all" value='0'/></div>
	<div><input type="submit" value="submit"></div>
</form>
</div>
<div>
<h1>analyse</h1>
<form action="/analyse" enctype="multipart/form-data" method="post">
	<div><label>text</label><textarea name='text'></textarea></div>
	<div><label>withWeight</label><input type="text" name="withWeight" value='0'/></div>
	<div><label>mode</label><input type="text" name="mode" value='TF-IDF'/></div>
	<div><label>topK</label><input type="text" name="topK" value='20'/></div>
	<div><input type="submit" value="submit"></div>
</form>
</div>
</body>
</html>
""")
		

app = webapp2.WSGIApplication([
	('/', Index),
	('/cut', Cut),
	('/analyse', Analyse)], debug=True)